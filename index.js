// Array Methods
	// JavaScript has built-in functions and methods for arrays. This allows us to manipultae and access array elements.

	// [SECTION] Mutator Methods
		// mutator methods are functions that mutate or change an array after they've created.
		// these methods manipulate the orginal array performing vrious takss as adding and removing elements.

let fruits = ['apple', 'orange', 'kiwi', 'dragon fruit'];


	// First mutator method: push()
		// adds an element/s in the end of an array and returns the array's length
		/*
			Syntax:
				arrayName.push(element/sToBeAdded);
		*/

console.log("Current array fruits []:");
console.log(fruits);

let fruitsLength = fruits.push("mango");
console.log("Mutated array after the push method:");
console.log(fruits);
console.log(fruitsLength);

// adding multiply elements to an array
fruitsLength = fruits.push('avocado', 'guava');
console.log("mutated array from the push method:")
console.log(fruits);
console.log(fruitsLength);

	// second mutator method: pop()
		// removes the last element in an array and returns the remove element
		/*
			Syntax:
				arrayName.pop();
		*/

console.log("Current array fruits []:");
console.log(fruits);

fruitsLength = fruits.pop();
console.log("mutated array after the pop method:");
console.log(fruits);
console.log(fruitsLength);

	// Third mutator method: unshift()
		// it adds one or more elements at the beginning of an array and returns the present length.
		/*
			Syntax:
				arrayName.unshift(element/sToBeAdded);
		*/

console.log("Current array fruits []:");
console.log(fruits);

fruitsLength = fruits.unshift("Lime", "Banana");
console.log("mutated array after the unshift method:");
console.log(fruits);
console.log(fruitsLength);

	//  fourth mutator method: shift()
		// removes an element at the beginning of an array and return the removed element.


console.log("Current array fruits []:");
console.log(fruits);

removedFruit = fruits.shift();
console.log("mutated array after the shift method:");
console.log(fruits);
console.log(removedFruit);

	// fifth mutator method: splice()
		// simultaneously removes elements from a specified index number and adds elements.
	/*
		Syntax:
			arrayName.splice(startingIndex, deleteCount, elementsToBeAdded);
	*/


console.log("Current array fruits []:");
console.log(fruits);

let splice = fruits.splice(1,1, "Lime");
console.log("mutated array after the splice method:");
console.log(fruits);
console.log(splice);

console.log("Current array fruits []:");
console.log(fruits);

fruits.splice(1,2);
console.log(fruits);

console.log("Current array fruits []:");
fruits.splice(2,0,"Durian", "Santol")
console.log(fruits);

	// sixth mutator method: sort()
		// rearrange the array elements in alphanumeric order
		/*
			Syntax:
			arrayName.sort();
		*/


console.log("Current array fruits []:");
console.log(fruits);

fruits.sort();
console.log("mutated array after the sort method:");
console.log(fruits);

	// seventh mutator method: reverse()
		// reverses the order of array elements
		/*
			Syntax:
				arrayName.reverse();
		*/


console.log("Current array fruits []:");
console.log(fruits);

fruits.reverse();
console.log("mutated array after the reverse method:");
console.log(fruits);

// [SECTION] non-mutator methods
	// non-mutator methods are functions that do not modify or change an array after they're created.
	// these methods do not manipulate the original array performing task such as returning arrays and printing the output.

let countries = ['us', 'ph', 'can','sg','th','ph','fr','de'];

	// first non-mutator method: indexOf()
		// it returns the index number of the first matching element found in an array
		// if no match was found the result will be -1
		// the search process will be done from first element proceeding to the last element
		/*
			Syntaxt:
				arrayName.indexOf(searchValue);
		*/

	console.log(countries);
	console.log(countries.indexOf("ph"));
	console.log(countries.indexOf("br"));

		// in indexOf() we can set the starting index
	console.log(countries.indexOf("ph",2));

	// second non-mutator method: lastIndexOf();
		// returns the index number of the last matching element found in an array.

	console.log(countries.lastIndexOf("ph"));

	// third non-mutator method: slice();
		// portion/slices from array and returns a new array
		/*
			Syntax:
				arrayName.slice(startingIndex);
				arrayName.slice(startingIndex, endingIndex);
		*/

	let slicedArrayA = countries.slice(2);
	console.log(slicedArrayA);
	console.log(countries);

	let slicedArrayB = countries.slice(1,5);
	console.log(slicedArrayB);

	// fourth non-mutator method: toString();
		// return an array as string separated by comma
		/*
			Syntax:
				arrayName.toString();
		*/

	let stringArray = countries.toString();
	console.log(stringArray);
	console.log(stringArray.length);

	// fifth non-mutator method: concat()
		// combines arrays and returns the combined result
		/*
			Syntax:
				arrayA.concat(ArrayB);
				arrayA.concat(elementA);
		*/

	let tasksArrayA = ["drink HTML", "eat JavaScript"];
	let tasksArrayB = ["inhale CSS", "breathe SASS"];
	let tasksArrayC = ["get git", "be node"];

	let tasks = tasksArrayB.concat(tasksArrayA);
	console.log(tasks);

	// adding/combining multiple arrays
	let allTalks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
	console.log(allTalks);

	// combine arrays with elements
	let combinedTasks = tasksArrayA.concat(tasksArrayB, "smell express", "throw react");
	console.log(combinedTasks);

	// sixth non-mutator method: join()
		// returns an array as string separated by specified separator string.
		/*
			Syntax:
				arrayName.join('separatorString');
		*/

	let users = ["john",'jane','joe','robert'];

	console.log(users.join());
	console.log(users.join(" "));
	console.log(users.join(" - "));

// [SECTION] Iteration Methods
	// iteration methods are loop designed to perform repetitive tasks on arrays
	// iteration method loop over all elements in an array.

	// first iteration method: forEach()
		// similar to for loop that iterates on each of array element
		// forEach element in the array, the fnction in the forEach method will be run.
		/*
			Syntax:
			arrayName.forEach(function(indivElement)) {
				statement/s;
			}
		*/

	console.log(allTalks);

	let filteredTask = [];
	allTalks.forEach(
		function(task) {
			if(task.length>10) {
				filteredTask.push(task)
			}

		}
	)

	console.log(filteredTask);

	// second iteration method: map()
		// iterates on each element and returns new array with different values depending on the resul of the function's operation
		/*
			Syntax:
				arrayName.map(
					function(element) {
						statement/s;
						return;
					}
				)
		*/

	let numbers = [1,2,3,4,5];
	console.log(numbers);

	let numberMap = numbers.map(
			function(numbers){
				return numbers*numbers;
			}	
		)
	console.log(numberMap);

	// third iteration method: every()
		// checks if all elements in an array meet the given condition.
		// this is useful in validating data stored in arrays specially when dealing with large amounts of data.
		// return true value f all elements meet the condition and false if otherwise
		/*
			Syntax:
				arrayName.every(
					function(element){
						return expression/condition;
					}
				)
		*/

	console.log(numbers);
	let allValid = numbers.every(
			function(numbers) {
				return (numbers<5);
			}
		)
	console.log(allValid);

	// fourth iteration method: some()
		// checks if at least one element in the array meets the given condition
		// returns a true value if at least one elemenet meets the condition and false if none.
		/*
			Syntax:
				arrayName.some(
					function(element){
						return expression/condition;
					}
				)
		*/


	console.log(numbers);
	let someValid = numbers.some(
			function(numbers) {
				return (numbers<0);
			}
		)
	console.log(someValid);

	// fifth iteration method: filter()
		// return a new array that contains the elements which meets the given condition
		// return empty array if no element were found
		/*
			Syntax:
				arrayName.filter(
					function(element) {
						return expression/condition;
					}
				)
		*/

	console.log(numbers);
	let filterValid = numbers.filter(
			function(numbers) {
				return (numbers <= 3);
			}
		)

	console.log(filterValid);

	// sixth iteration method: includes()
		// checks if the argument passed can be found in the array
		// it returns boolean which can be saved in a variable
			// return true if the argument is found in the array
			// ruturn false if not found.
			/*
				Syntax:
					arrayName.includes(argument);
			*/

	let products = ["mouse", "keyboard", "laptop", "monitor"];
	let productFound1 = products.includes("mouse");
	console.log(productFound1);

	let productFound2 = products.includes("Mouse");
	console.log(productFound2);

	// seventh iteration method: reduce()
		// evaluate elements from left to right and returns the reduce array.
		// array will turn into single value
		/*
			Syntax:
				arrayName.reduce(
					function(accumulator,currentValue) {
						return operation/expression;
					}
				)
		*/

	console.log(numbers);

	let total = numbers.reduce(
			function(x,y) {
				console.log("this is the value of x: "+x);
				console.log("this is the value of y: "+y);
				return x+y;
			}
		)

	console.log(total);